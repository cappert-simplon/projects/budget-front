# Projet Budget - Front End
L'objectif de ce projet est de créer une application de gestion de budget en front en utilisant HTML/CSS et Typescript

## Fonctionnalités principales
L'application doit permettre de gérer son budget : Indiquer les différentes sorties (et éventuellement entrées) d'argent avec le montant, la catégorie de dépense/entrée et un petit titre si on souhaite le préciser.

### Diagramme de Use case
![diagramme de use case](budget-use-case.png)

### Fonctionnalités Additionnelles
* Permettre de sauvegarder le budget actuel en localStorage
* Permettre d'accéder à un graphique de nos dépenses par mois (sous forme de pie chart par exemple)
* Gérer les catégories indépendamment des opérations (dans leur propre entité)
* Indiquer un budget à ne pas dépasser par mois avec un code couleur quand on s'approche de ce budget ou qu'on le dépasse

## Travail attendu
* Créer une maquette fonctionnelle de l'application
* Créer une ou des entités en typescript (avec interface ou classe)
* Faire une UI responsive en utilisant bootstrap (ou autre)
* Rendre l'UI interactive (dynamique) en utilisant typescript
* (Optionnel, mais intéressant) Essayer de reproduire des logiques de MVC sur l'application, en faisant une séparation entre le modèle de données et l'affichage

Pour le projet de budget, ce qui est attendu :
  - une page avec une liste de transactions
  - la possibilité d'ajouter des transactions
  - des entités en TypeScript
  - un jeu d'essai dans vos scripts
  - la manipulation du DOM et du CSS suivant les méthodes vues en formation
  - une bonne organisation de vos fichiers projets, un Readme clair et bien construit
