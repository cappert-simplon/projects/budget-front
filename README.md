# Simple Budget - Camille A.

design en deux colonnes fortement inspiré de bankin.com

## organisation HTML
### dans la colonne de gauche (.transactionList)

liste des transactions (.transactionRow), remplie automatiquement au chargement de la page, et un bouton pour ajouter une transaction.

### dans la colonne de droite (.transactionDetail)
affiche soit la transaction selectionné soit le formulaire pour ajouter une transaction (.modal) 

## organisation Typescript

`src/main.ts`:

point d'entrée de l'app.
* remplie `transactionList` de `transactionRow`s à partir du jeu de données (`seeds.ts`)
* addEventListener sur le bouton `addItem` (dans `transactionList`) pour afficher le formulaire d'ajout
* addEventListener sur le bouton de soumission du formulaire d'ajout `submitItem`

`src/createTransactionRow.ts`:

fonction de création d'une row dans le DOM à partir d'une entité Transaction.

contient également le addEventListener du clic sur une row, qui l'affiche dans `transactionDetail`.

`src/helpers.ts`: 
* decorateWithCurrency() - pour l'instant ajoute le symbole Euro au `transactionAmount`, plus tard on pourrait faire varier selon la devise (symbole Dollar avant le nombre par exemple).
* moveActive() - appelée par les eventListener pour déplacer la classe `.active` pour styler l'élément sélectionné.

`src/modal.ts`: 

fonctions pour afficher et cacher le modal (.transactionDetail), pourrait aller dans `helpers.ts`

`src/seeds.ts`:

Jeu de données (Catégories, quelques Transactions)

`src/entities/transaction.ts`

    label:string;
    amount:number;
    category:number;

`src/entities/category.ts`

    id:number;
    label:string;

Contient du code de

https://getbootstrap.com/docs/5.1/examples/sidebars/

https://getbootstrap.com/docs/5.1/examples/modals/

par Mark Otto, Jacob Thornton, et contributeurs Bootstrap
(MIT License)
