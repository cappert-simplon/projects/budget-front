import './style.css'

import { transactions } from "./seeds";
import createTransactionRow from './createTransactionRow';
import { showModal } from './modal';
import { moveActive } from './helpers';
import { Transaction } from "./entities/transaction";


const listGroup = document.querySelector<HTMLElement>('#transactionList .list-group')!;
const showAddItem = document.querySelector<HTMLElement>('#add-item')!;

for (const transaction of transactions) {
  let transactionRow = createTransactionRow({id: transaction.id, label: transaction.label, amount: transaction.amount, category: transaction.category});
  listGroup.appendChild(transactionRow);

}

showAddItem.addEventListener('click', (event) => {
  addItemEvent(event);
});

let submitItem = document.querySelector<HTMLElement>('#modal [type=submit]')!;

submitItem.addEventListener('click', (event) => {
  let form = document.querySelector<HTMLFormElement>('#modal form')!;
  // https://stackoverflow.com/q/48897314
  if (!form.checkValidity()) {
    form.reportValidity();
  } else {
  event.preventDefault();

  const transactionLabel = document.querySelector<HTMLInputElement>('#input-label')!.value;
  const transactionAmount = document.querySelector<HTMLInputElement>('#input-amount')!.value;
  const transactionCategory = document.querySelector<HTMLInputElement>('#input-category')!.value;

  const transactionId = Transaction.createTransactionId();

  // une autre façon de faire par rapport à ligne 15

  const transaction = new Transaction(
      transactionId, transactionLabel, Number(transactionAmount), Number(transactionCategory));

  listGroup.prepend(createTransactionRow(transaction));
  
  // clear form (HTMLInputElement works with select too ¯\_(ツ)_/¯ we only use this Type to use .value anyway)
  form.querySelectorAll<HTMLInputElement>('input, select').forEach(input => {
    input.value = '';
  });

  }
})
export function addItemEvent(event: MouseEvent) {
  event.preventDefault();

  moveActive(showAddItem);

  let transactionDetail = document.querySelector<HTMLElement>('#transactionDetail')!;

  // open modal
  transactionDetail.innerHTML = '';
  showModal();
}

export function createCloseButton(): HTMLButtonElement {
  let closeButton = document.createElement('button');
  closeButton.classList.add('close-button');
  closeButton.textContent = 'Fermer';

  closeButton.addEventListener('click', (event)=> {
    addItemEvent(event)
  });
  
  return closeButton;
}
