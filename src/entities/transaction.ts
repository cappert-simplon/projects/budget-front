import { counterSeed } from "../seeds";

export class Transaction {
    id:string;
    label:string;
    amount:number;
    category:number;
    
    static counterIncrement:number = counterSeed;
    
    constructor(id:string, label:string, amount:number, category: number) {
        this.id = id;
        this.label = label;
        this.amount = amount;
        this.category = category;
    }

    static createTransactionId(): string {
        return String(Transaction.counterIncrement++) + '-' +  Date.now(); 
    }
}
