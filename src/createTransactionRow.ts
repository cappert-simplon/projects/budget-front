import { decorateWithCurrency, moveActive } from "./helpers";
import { Transaction } from "./entities/transaction";
import { hideModal } from "./modal";
import { categories } from "./seeds";
import { createCloseButton } from "./main";

// src/templates/transactionRow.html
export default function createTransactionRow(transaction: Transaction) {

  let transactionRow = document.createElement('a');
  transactionRow.classList.add("transactionRow", "list-group-item", "list-group-item-action", "py-3", "lh-tight");

  let colorHr = document.createElement('hr');
  colorHr.classList.add('category-color-' + transaction.category.toString())

  let dFlex = document.createElement('div');
  dFlex.classList.add('d-flex', 'w-100', 'align-items-center', 'justify-content-between');

  // transactionLabel
  let transactionLabel = document.createElement('strong');
  transactionLabel.classList.add('transactionLabel', 'mb-1');
  transactionLabel.textContent = transaction.label;

  // transactionAmount
  let transactionAmount = document.createElement('strong');
  transactionAmount.classList.add('transactionAmount');

  transactionAmount.textContent = decorateWithCurrency(transaction.amount)

  dFlex.appendChild(transactionLabel);
  dFlex.appendChild(transactionAmount);

  // transactionCategory (direct child of TransactionRow)
  let transactionCategory = document.createElement('div');
  transactionCategory.classList.add('col-10', 'mb-1', 'small');
  // transaction.category - 1 because it's in a zero-indexed array but starts with id: 1
  transactionCategory.textContent = categories[transaction.category - 1].label;

  transactionRow.appendChild(dFlex);
  transactionRow.appendChild(transactionCategory);
  transactionRow.appendChild(colorHr);
  
  let transactionId = Transaction.createTransactionId()
  transactionRow.id = transactionId;
  transactionRow.href = '#t' + transactionId;

    // onclick show Transaction in transactionDetail
    transactionRow.addEventListener('click', (event)=> {
      event.preventDefault();

      // createTransactionDetail(Transaction transaction) { ??
      let transactionDetail = document.querySelector<HTMLElement>('#transactionDetail')!;

      let newTransactionDetail = document.createElement('div');
      newTransactionDetail.id = 'transactionDetail';
      newTransactionDetail.classList.add('pt-3', 'col-12', 'col-sm-8');

      let newTransactionLabel = document.createElement('h2');
      let newTransactionAmount = document.createElement('p');
      let newtransactionCategory = document.createElement('strong');

      newTransactionLabel.textContent = transaction.label;
      newTransactionAmount.textContent = decorateWithCurrency(transaction.amount);
      
      newtransactionCategory.textContent = categories[transaction.category - 1].label;

      newtransactionCategory.classList.add('category-color-' + transaction.category.toString());

      let closeButton = createCloseButton();


      newTransactionDetail.appendChild(newTransactionLabel);
      newTransactionDetail.appendChild(newTransactionAmount);
      newTransactionDetail.appendChild(newtransactionCategory);
      newTransactionDetail.appendChild(closeButton);
      
      moveActive(transactionRow);

      // hide the addItem form
      hideModal();

      transactionDetail.replaceWith(newTransactionDetail);

    })
  
  return transactionRow;
}


