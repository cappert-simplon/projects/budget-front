export function hideModal() {
  let modal = document.querySelector<HTMLDivElement>('#modal')!
  let transactionDetail = document.querySelector<HTMLDivElement>('#transactionDetail')!

  modal.style.display = 'none';
  transactionDetail.classList.remove('d-none','d-sm-none')
  
  //
}

export function showModal() {
  let modal = document.querySelector<HTMLDivElement>('#modal')!
  let transactionDetail = document.querySelector<HTMLDivElement>('#transactionDetail')!

  
  modal.style.display = 'block';
  transactionDetail.classList.add('d-none','d-sm-none');

}
