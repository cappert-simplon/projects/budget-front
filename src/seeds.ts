import { Transaction } from "./entities/transaction";
import { Category } from "./entities/category";

export const categories: Category[] = [
   {id: 1,  label:"Abonnements"},
   {id: 2,  label:"Achats & Shopping"},
   {id: 3,  label:"Alimentation & Restau."},
   {id: 4,  label:"Auto & Transports"},
   {id: 5,  label:"Banque"},
   {id: 6,  label:"Esthétique & Soins"},
   {id: 7,  label:"Impôts & Taxes"},
   {id: 8,  label:"Logement"},
   {id: 9,  label:"Loisirs & Sorties"},
   {id: 10, label: "Retraits, Chq. et Vir."},
   {id: 11, label: "Santé"},
   {id: 12, label: "Divers"}
]

export let transactions: Transaction[] = [
    { id: '1-1653020484000', label: 'SFR', amount: 23, category: 1  },
    { id: '2-1652329284000', label: 'Intermarché', amount:13.35, category: 3 },
    { id: '3-1652070084000', label: 'EDF', amount:22, category: 8 },
    { id: '4-1649535684000', label: 'Afk Bar', amount:5.5, category: 9 },
    { id: '5-1651580484000', label: 'Miette Boulange', amount:4.7, category: 3 },
    { id: '6-1651850064000', label: 'Pharmacie Roosevelt', amount:13.86, category: 11 },
    { id: '7-1651672764000', label: 'L\'atelier', amount:4.5, category: 3 },
    { id: '8-1649013940000', label: 'Cga', amount:3.5, category: 12 },
]  

export let counterSeed:number = 9;
