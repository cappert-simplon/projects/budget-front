export function decorateWithCurrency(amount: number): string {
  //                    non-breaking space, html entities are escaped
  return amount.toString() + ' €';
}

export function moveActive(element: HTMLElement) {
  document.querySelector<HTMLElement>('.active')?.classList.remove('active');
  element.classList.add('active');
}
